!/bin/sh

# Script for installing base packages and activating certain daemons and services
# This is only for having a very basic DE and utils to later scale up from

echo "Installing packages..."
# install packages, i3 (gaps, bar, status), xorg, pipewire, repo NON-FREE
#xbps-install git bat fzf exa zsh rxvt-unicode i3 i3-gaps i3-bar i3status rofi dbus xorg pipewire libpulseaudio-pipewire apvlv sxiv socklog-void newsboat firefox neovim nodejs python cargo docker atop ranger pcmanfm pulseaudio-utils nitrogen void-repo-nonfree dunst &&

# install packages, bspwm + lemonbar, xorg, pipewire, repo NON-FREE
xbps-install dbus git bat fzf exa zsh xorg rxvt-unicode lemonbar bspwm rofi pipewire libpulseaudio-pipewire apvlv sxiv socklog-void newsboat firefox neovim nodejs python cargo docker atop ranger pcmanfm pulseaudio-utils nitrogen void-repo-nonfree dunst &&

echo -e "Done! (1 / 3)\n Installing non-free packages..."
# install nonfree packages
xbps-install opera chromium &&

echo -e "Done! (2 / 3)\n Activating daemons and services..."
# soft symbolic links to enable services via runit
ln -s /etc/sv/dbus /var/service/ &
ln -s /etc/sv/nanoklogd /var/service/ &
ln -s /etc/sv/socklog-unix /var/service/ &&

echo -e "Done! (3 / 3)\n Script ended with code $?"
